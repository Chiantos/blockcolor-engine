--
-- Aliases for map generators
--

minetest.register_alias("mapgen_stone","color:black")

minetest.register_alias("mapgen_dirt","color:black")

minetest.register_alias("mapgen_dirt_with_grass","color:black")

minetest.register_alias("mapgen_sand","color:black")

minetest.register_alias("mapgen_water_source","water:blue_water_source")

minetest.register_alias("mapgen_river_water_source","water:blue_water_source")

minetest.register_alias("mapgen_lava_source","water:red_water_source")

minetest.register_alias("mapgen_gravel","color:black")

minetest.register_alias("mapgen_desert_stone","color:black")

minetest.register_alias("mapgen_desert_sand","color:black")

minetest.register_alias("mapgen_dirt_with_snow","color:black")

minetest.register_alias("mapgen_snowblock","color:black")

minetest.register_alias("mapgen_snow","color:black")

minetest.register_alias("mapgen_ice","color:black")

minetest.register_alias("mapgen_sandstone","color:black")

-- Flora

minetest.register_alias("mapgen_tree", "color:black")
minetest.register_alias("mapgen_leaves", "color:black")
minetest.register_alias("mapgen_apple", "color:black")
minetest.register_alias("mapgen_jungletree", "color:black")
minetest.register_alias("mapgen_jungleleaves", "color:black")
minetest.register_alias("mapgen_junglegrass","color:black")
minetest.register_alias("mapgen_pine_tree", "color:black")
minetest.register_alias("mapgen_pine_needles", "color:black")
-- Dungeons

minetest.register_alias("mapgen_cobble", "color:black")
minetest.register_alias("mapgen_stair_cobble", "color:black")
minetest.register_alias("mapgen_mossycobble", "color:black")
minetest.register_alias("mapgen_stair_desert_stone", "color:black")
minetest.register_alias("mapgen_sandstonebrick", "color:black")
minetest.register_alias("mapgen_stair_sandstone_block", "color:black")

--
-- Register biomes
--

-- All mapgens except mgv6

function default.register_biomes(upper_limit)

	-- White Biome (Cold)

	minetest.register_biome({
		name = "moon",
		node_top = "comboblock:slab_black_onc_slab_black",
		depth_top = 1,
		node_filler = "comboblock:slab_black_onc_slab_black",
		depth_filler = 1,
		node_stone = "comboblock:slab_black_onc_slab_black",
		node_riverbed = "comboblock:slab_black_onc_slab_black",
		depth_riverbed = 2,
		y_min = 5,
		y_max = upper_limit,
		heat_point = 20,
		humidity_point = 35,
	})


end

--
-- Register decorations
--

function default.register_decorations()
	
end


--
-- Detect mapgen, flags and parameters to select functions
--

-- Get setting or default
local mgv7_spflags = minetest.get_mapgen_setting("mgv7_spflags") or
	"mountains, ridges, nofloatlands"
local captures_float = string.match(mgv7_spflags, "floatlands")
local captures_nofloat = string.match(mgv7_spflags, "nofloatlands")

local mgv7_floatland_level = minetest.get_mapgen_setting("mgv7_floatland_level") or 1280
local mgv7_shadow_limit = minetest.get_mapgen_setting("mgv7_shadow_limit") or 1024

minetest.clear_registered_biomes()
minetest.clear_registered_ores()
minetest.clear_registered_decorations()

local mg_name = minetest.get_mapgen_setting("mg_name")

if mg_name == "v7" and captures_float == "floatlands" and
		captures_nofloat ~= "nofloatlands" then
	-- Mgv7 with floatlands
	default.register_biomes(mgv7_shadow_limit - 1)
	default.register_floatland_biomes(mgv7_floatland_level, mgv7_shadow_limit)
	default.register_decorations()
else
	default.register_biomes(31000)
	default.register_decorations()
end
